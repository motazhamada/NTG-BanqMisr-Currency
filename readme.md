# Currency Android Application

Currency is an Android application that allows users to convert currencies from different bases. 
It follows the MVVM architectural pattern with Clean Architecture principles, ensuring a clear separation of concerns and enhancing modularity and testability.

## Table of Contents
1. [Introduction](#introduction)
2. [Technology Stack](#technology-stack)
3. [Requirements](#requirements)
4. [Getting Started](#getting-started)
5. [Project Structure](#project-structure)
6. [Architecture](#architecture)
7. [Features](#features)
8. [Testing](#testing)

## Introduction

Currency is a multi-module Android application written in Kotlin using the Jetpack MVVM architecture, KTX, Data Binding, Navigation Component, Hilt for dependency injection, Coroutines for asynchronous programming, and Retrofit for API communication.
The app allows users to convert currencies from one base to another, view historical exchange rate data, and check conversions for popular currencies.

## Technology Stack

- Kotlin
- Jetpack (MVVM, ViewModel, LiveData, Data Binding, Navigation Component)
- Hilt (Dependency Injection)
- Coroutines (Asynchronous Programming)
- Retrofit (Network Communication)
- Gson (JSON Parsing)
- MPAndroidChart (Chart Visualization)
- JUnit 5 and MockK (Unit Testing)
- Version Catalog

## Requirements

- Android Studio 4.0+
- Gradle 7.0+
- Android API level 26+
- Internet connection to fetch live data

## Getting Started

1. Clone the repository from GitHub: `git clone https://gitlab.com/motazhamada/NTG-BanqMisr-Currency.git`
2. Open the project in Android Studio.
3. Build and run the application on a device or emulator.

## Project Structure

The project follows the multi-module structure with a feature module called "exchange" that encapsulates the data, domain, and presentation layers. Here's a brief overview of the modules:

- **app**: Contains the main Android application and acts as the entry point to the application. It includes the UI and View components for the currency exchange feature.
- **exchange**: The feature module that contains the data, domain, and presentation layers for currency exchange functionality.
  - **data**: Handles data-related operations, including API communication. It also includes the network implementation.
  - **domain**: Defines the core business logic and entities related to currency exchange.
  - **presentation**: Contains the ViewModel and state management for the currency exchange feature.

The "exchange" module represents the main functionality of the Currency Converter app and is structured to follow the Clean Architecture principles, ensuring a clear separation of concerns and enhancing modularity.

## Architecture

The application follows the Clean Architecture design pattern, ensuring a clear separation of concerns between different layers: domain, data, and presentation. This separation enhances modularity and testability.

The flow of data within the "exchange" module follows these steps:

1. Presentation layer interacts with ViewModel for user actions and updates UI.
2. ViewModel uses UseCases from the domain layer to execute business logic.
3. UseCases interact with the data layer through repositories to fetch data from APIs.

By organizing the application into feature modules, the project remains scalable and maintainable as new features or modules can be added independently, reducing the risk of code coupling and improving code reuse.

## Features

### Convert Currency

- Users can convert currencies from different bases.
- Dropdown lists display all available currencies.
- An input field allows users to enter the amount to be converted.
- The converted value updates automatically on amount change.
- A button allows users to swap the values between FROM and TO currencies, updating the converted data accordingly.

### Other Currencies

- Users can view conversion rates to 10 different popular currencies.
- Historical Data Chart (Bonus)

### Historical Data

- Users can view historical data for the selected FROM/TO currencies for the last 3 days.
- Historical data is presented in a list view and optionally on a chart for better visualization.

## Testing

Unit tests are provided for all applicable areas of the application, including ViewModel and UseCase testing. The tests ensure the correctness and reliability of the codebase.

The Currency Converter application has been developed following the provided requirements and guidelines. It features a precise error handling mechanism to differentiate between internet connection errors and API errors. The ViewModel has been designed as a shared ViewModel between the two fragments, promoting simplicity and maintainability. The application also includes unit tests to ensure the correctness and reliability of the codebase.

The project adheres to modern Android development best practices, using Jetpack components, Kotlin, and Coroutines for asynchronous operations. It follows a clean code architecture with a clear separation of concerns, enhancing modularity and testability. The user interface is simple and intuitive, ensuring a smooth user experience for currency conversions and historical data exploration.

Feel free to explore the codebase and the commits in the provided Git repository to see the progress and development process. If you have any questions or feedback, please don't hesitate to reach out.