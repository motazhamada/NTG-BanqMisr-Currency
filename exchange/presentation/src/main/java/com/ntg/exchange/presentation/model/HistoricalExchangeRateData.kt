package com.ntg.exchange.presentation.model
data class HistoricalExchangeRateData(
    val date: String,
    val rates: List<CurrencyConversion>,
)
