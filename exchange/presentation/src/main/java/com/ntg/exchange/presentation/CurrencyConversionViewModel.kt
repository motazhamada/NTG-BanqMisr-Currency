package com.ntg.exchange.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ntg.exchange.core.network.ConnectivityObserver
import com.ntg.exchange.domain.useCases.GetExchangeRatesUseCase
import com.ntg.exchange.domain.useCases.GetHistoricalExchangeRatesUseCase
import com.ntg.exchange.presentation.mapper.toPresentationModel
import com.ntg.exchange.presentation.model.CurrencyConversion
import com.ntg.exchange.presentation.model.HistoricalExchangeRateData
import com.ntg.exchange.presentation.state.ExchangeRateDataState
import com.ntg.exchange.presentation.state.HistoricalExchangeRateDataState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale
import javax.inject.Inject

@HiltViewModel
class CurrencyConversionViewModel @Inject constructor(
    private val getExchangeRatesUseCase: GetExchangeRatesUseCase,
    private val getHistoricalExchangeRatesUseCase: GetHistoricalExchangeRatesUseCase,
    private val connectivityObserver: ConnectivityObserver,
) : ViewModel() {

    init {
        connectivityObserver.registerNetworkCallback()
    }

    val isConnected: LiveData<Boolean>
        get() = connectivityObserver.isConnected

    private val _exchangeRatesDataState: MutableStateFlow<ExchangeRateDataState> =
        MutableStateFlow(ExchangeRateDataState.Initial)
    val exchangeRatesDataState: StateFlow<ExchangeRateDataState>
        get() = _exchangeRatesDataState.asStateFlow()

    private val _historicalExchangeRatesState: MutableStateFlow<HistoricalExchangeRateDataState> =
        MutableStateFlow(HistoricalExchangeRateDataState.Initial)
    val historicalExchangeRatesState: StateFlow<HistoricalExchangeRateDataState>
        get() = _historicalExchangeRatesState.asStateFlow()

    // LiveData to hold the list of currencies fetched from the API
    private val _currencies = MutableLiveData<List<CurrencyConversion>>()
    val currenciesNames: LiveData<List<String>?>
        get() = MediatorLiveData<List<String>?>().apply {
            // Set up the mediator to update the list of currency names whenever _currencies changes
            addSource(_currencies) { currencies ->
                value = currencies?.map { it.currency }
            }
        }

    // LiveData to hold the selected currencies
    private val _currencyFrom = MutableLiveData<String>()
    val currencyFrom: LiveData<String>
        get() = _currencyFrom

    private val _currencyTo = MutableLiveData<String>()
    val currencyTo: LiveData<String>
        get() = _currencyTo

    var amountFrom: String = INITIAL_CURRENCY_AMOUNT.toString()
        set(value) {
            field = value
            convertCurrencies()
        }

    private val _amountTo = MutableLiveData<String>()
    val amountTo: LiveData<String>
        get() = _amountTo

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean>
        get() = _isLoading

    private val _historicalExchangeRates =
        MutableLiveData<MutableList<HistoricalExchangeRateData>>()
        get() {
            if (field.value == null) {
                field.value = mutableListOf()
            }
            return field
        }

    private var lastUpdateTime: Long = 0L

    fun setLoadingState(value: Boolean) {
        _isLoading.value = value
    }

    private val currencyFromItemSelectedListener = object : OnItemSelectedListener {
        override fun onItemSelected(item: String) {
            _currencyFrom.value = item
            // Recalculate the converted amount whenever the currency selection changes
            convertCurrencies()
        }
    }

    private val currencyToItemSelectedListener = object : OnItemSelectedListener {
        override fun onItemSelected(item: String) {
            _currencyTo.value = item
            // Recalculate the converted amount whenever the currency selection changes
            convertCurrencies()
        }
    }

    // Getters for the currency selection listeners
    fun getCurrencyFromListener(): OnItemSelectedListener = currencyFromItemSelectedListener

    fun getCurrencyToListener(): OnItemSelectedListener = currencyToItemSelectedListener

    // Method to handle the "Swap Currencies" button click
    fun onSwapCurrenciesClicked() {
        if (_currencyFrom.value != null && _currencyTo.value != null && _currencyFrom.value != _currencyTo.value) {
            val tempCurrencyFrom = _currencyFrom.value
            _currencyFrom.value = _currencyTo.value
            _currencyTo.value = tempCurrencyFrom!!

            // Recalculate the converted amount whenever the currencies are swapped
            convertCurrencies()
        }
    }

    // Method to calculate the converted amount based on the selected currencies and amount values
    private fun convertCurrencies() {
        val amount = amountFrom.toDoubleOrNull() ?: INITIAL_CURRENCY_AMOUNT
        val rateFrom = getRateForCurrency(_currencyFrom.value) ?: INITIAL_CONVERT_RATE
        val rateTo = getRateForCurrency(_currencyTo.value) ?: INITIAL_CONVERT_RATE

        val convertedAmount = amount * rateTo / rateFrom
        _amountTo.value = String.format("%.2f", convertedAmount)
    }

    private fun getRateForCurrency(currency: String?): Double? {
        return _currencies.value?.firstOrNull { it.currency == currency }?.amount
    }

    fun performGetExchangeRatesData() {
        // updating if the data was fetched since more than 5 minutes
        val currentTime = System.currentTimeMillis()
        if (!_currencies.value.isNullOrEmpty() && (currentTime - lastUpdateTime) >= 5 * 60 * 1000) {
            return
        }
        viewModelScope.launch {
            try {
                // Check network connection before proceeding
                if (isConnected.value == false) {
                    _exchangeRatesDataState.emit(ExchangeRateDataState.Error(Throwable("No internet connection")))
                    return@launch
                }
                _exchangeRatesDataState.emit(ExchangeRateDataState.Loading)
                val result = getExchangeRatesUseCase()
                result?.toPresentationModel()?.let { exchangeRates ->
                    lastUpdateTime = System.currentTimeMillis()
                    _exchangeRatesDataState.emit(
                        ExchangeRateDataState.Success(exchangeRates.rates).also {
                            _currencies.value = it.rates
                        },
                    )
                } ?: _exchangeRatesDataState.emit(
                    ExchangeRateDataState.Error(
                        Throwable("Null data returned"),
                    ),
                )
            } catch (e: Exception) {
                _exchangeRatesDataState.emit(ExchangeRateDataState.Error(e))
            }
        }
    }

    fun performGetHistoricalExchangeRatesData() {
        if (_historicalExchangeRates.value?.isEmpty() == false) {
            return
        }
        viewModelScope.launch {
            try {
                // Check network connection before proceeding
                if (isConnected.value == false) {
                    _exchangeRatesDataState.emit(ExchangeRateDataState.Error(Throwable("No internet connection")))
                    return@launch
                }
                _historicalExchangeRatesState.emit(HistoricalExchangeRateDataState.Loading)

                val dateRange = generateDateRangeForLast3Days()

                for (date in dateRange) {
                    val result = getHistoricalExchangeRatesUseCase(date)
                    result?.toPresentationModel()?.let { exchangeRates ->
                        val historicalExchangeRateData =
                            HistoricalExchangeRateData(date, exchangeRates.rates)
                        _historicalExchangeRates.value?.add(0, historicalExchangeRateData)
                    }
                }

                if (_historicalExchangeRates.value?.isNotEmpty() == true) {
                    _historicalExchangeRatesState.emit(
                        HistoricalExchangeRateDataState.Success(
                            _historicalExchangeRates.value!!,
                        ),
                    )
                } else {
                    _historicalExchangeRatesState.emit(
                        HistoricalExchangeRateDataState.Error(
                            Throwable("No data returned"),
                        ),
                    )
                }
            } catch (e: Exception) {
                _historicalExchangeRatesState.emit(HistoricalExchangeRateDataState.Error(e))
            }
        }
    }

    // Function to generate date range for last 3 days
    private fun generateDateRangeForLast3Days(): List<String> {
        val dateList = mutableListOf<String>()
        val calendar = Calendar.getInstance()

        for (i in 0 until COUNT_OF_HISTORICAL_DAYS) {
            val date = SimpleDateFormat("yyyy-MM-dd", Locale.US).format(calendar.time)
            dateList.add(date)
            calendar.add(Calendar.DAY_OF_MONTH, -1)
        }

        return dateList
    }

    companion object {
        const val INITIAL_CURRENCY_AMOUNT = 1.0
        const val DEBOUNCE_TIME = 1000L
        private const val INITIAL_CONVERT_RATE = 1.0
        private const val COUNT_OF_HISTORICAL_DAYS = 3
    }

    override fun onCleared() {
        super.onCleared()
        connectivityObserver.unregisterNetworkCallback()
    }
}

interface OnItemSelectedListener {
    fun onItemSelected(item: String)
}
