package com.ntg.exchange.presentation.mapper

import com.ntg.exchange.data.model.ExchangeRateDTO
import com.ntg.exchange.presentation.model.CurrencyConversion
import com.ntg.exchange.presentation.model.ExchangeRates
import java.math.BigDecimal
import java.math.RoundingMode

fun ExchangeRateDTO.toPresentationModel(): ExchangeRates {
    return ExchangeRates(
        base = base,
        timestamp = timestamp,
        rates = rates.toListCurrencyConversion(),
    )
}

private fun Map<String, Double>.toListCurrencyConversion(): List<CurrencyConversion> {
    return map {
        CurrencyConversion(
            currency = it.key.uppercase(),
            amount = it.value.roundToThreeDigitsAfterDecimal(),
        )
    }
}

fun Double.roundToThreeDigitsAfterDecimal(): Double {
    val rounded = BigDecimal(this.toString()).setScale(3, RoundingMode.HALF_UP)
    return rounded.toDouble()
}
