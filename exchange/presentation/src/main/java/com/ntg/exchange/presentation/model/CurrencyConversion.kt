package com.ntg.exchange.presentation.model

data class CurrencyConversion(val currency: String, var amount: Double)
