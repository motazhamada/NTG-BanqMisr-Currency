package com.ntg.exchange.presentation.model

data class ExchangeRates(
    val timestamp: Int,
    val base: String,
    val rates: List<CurrencyConversion>,
)
