package com.ntg.exchange.presentation.state

import com.ntg.exchange.presentation.model.HistoricalExchangeRateData

sealed class HistoricalExchangeRateDataState {
    object Initial : HistoricalExchangeRateDataState()
    object Loading : HistoricalExchangeRateDataState()
    class Success(val data: List<HistoricalExchangeRateData>) : HistoricalExchangeRateDataState()
    class Error(val error: Throwable) : HistoricalExchangeRateDataState()
}
