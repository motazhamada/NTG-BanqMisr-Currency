package com.ntg.exchange.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.ntg.exchange.core.network.ConnectivityObserver
import com.ntg.exchange.domain.useCases.GetExchangeRatesUseCase
import com.ntg.exchange.domain.useCases.GetHistoricalExchangeRatesUseCase
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestDispatcher
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class CurrencyConversionViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel: CurrencyConversionViewModel
    private lateinit var mockGetExchangeRatesUseCase: GetExchangeRatesUseCase
    private lateinit var mockGetHistoricalExchangeRatesUseCase: GetHistoricalExchangeRatesUseCase
    private lateinit var mockConnectivityObserver: ConnectivityObserver
    private lateinit var testDispatcher: TestDispatcher

    @Before
    fun setUp() {
        testDispatcher = UnconfinedTestDispatcher()
        Dispatchers.setMain(testDispatcher)

        mockGetExchangeRatesUseCase = mockk()
        mockGetHistoricalExchangeRatesUseCase = mockk()
        mockConnectivityObserver = mockk(relaxed = true)

        viewModel = CurrencyConversionViewModel(
            mockGetExchangeRatesUseCase,
            mockGetHistoricalExchangeRatesUseCase,
            mockConnectivityObserver,
        )
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `setLoadingState() should update the isLoading LiveData`() {
        // Given
        val isLoadingValue = true

        // When
        viewModel.setLoadingState(isLoadingValue)

        // Then
        assertEquals(isLoadingValue, viewModel.isLoading.value)
    }
}
