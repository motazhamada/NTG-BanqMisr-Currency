package com.ntg.exchange.domain.di

import com.ntg.exchange.core.DefaultDispatcher
import com.ntg.exchange.data.ExchangeRateRepository
import com.ntg.exchange.domain.useCases.GetExchangeRatesUseCase
import com.ntg.exchange.domain.useCases.GetExchangeRatesUseCaseImpl
import com.ntg.exchange.domain.useCases.GetHistoricalExchangeRatesUseCase
import com.ntg.exchange.domain.useCases.GetHistoricalExchangeRatesUseCaseImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.CoroutineDispatcher

@Module
@InstallIn(ViewModelComponent::class)
object UseCasesDiModule {

    @Provides
    @ViewModelScoped
    fun provideGetExchangeRatesUseCase(
        repository: ExchangeRateRepository,
        @DefaultDispatcher dispatcher: CoroutineDispatcher,
    ): GetExchangeRatesUseCase =
        GetExchangeRatesUseCaseImpl(repository, dispatcher)

    @Provides
    @ViewModelScoped
    fun provideGetHistoricalExchangeRatesUseCase(
        repository: ExchangeRateRepository,
        @DefaultDispatcher dispatcher: CoroutineDispatcher,
    ): GetHistoricalExchangeRatesUseCase =
        GetHistoricalExchangeRatesUseCaseImpl(repository, dispatcher)
}
