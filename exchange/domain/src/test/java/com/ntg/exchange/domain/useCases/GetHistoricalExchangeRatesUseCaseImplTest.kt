package com.ntg.exchange.domain.useCases

import com.ntg.exchange.data.ExchangeRateRepository
import com.ntg.exchange.data.model.ExchangeRateDTO
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class GetHistoricalExchangeRatesUseCaseImplTest {

    @Test
    fun `invoke should return exchange rate DTO from repository`() = runTest {
        // Given
        val date = "2023-08-01"
        val expectedExchangeRateDTO = mockk<ExchangeRateDTO>()
        val repository = mockk<ExchangeRateRepository> {
            coEvery { getHistoricalExchangeRates(date) } returns expectedExchangeRateDTO
        }
        val useCase = GetHistoricalExchangeRatesUseCaseImpl(repository, Dispatchers.Default)
        // When
        val result = useCase.invoke(date)

        // Then
        assertEquals(expectedExchangeRateDTO, result)
    }

    @Test
    fun `invoke should return null if an exception is thrown in repository`() = runBlocking {
        // Given
        val date = "2023-08-01"
        val repository = mockk<ExchangeRateRepository> {
            coEvery { getHistoricalExchangeRates(date) } throws Exception("Error fetching data")
        }
        val useCase = GetHistoricalExchangeRatesUseCaseImpl(repository, Dispatchers.Default)

        // When
        val result = useCase.invoke(date)

        // Then
        assertEquals(null, result)
    }
}
