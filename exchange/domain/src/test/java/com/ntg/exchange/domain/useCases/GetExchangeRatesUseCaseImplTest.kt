package com.ntg.exchange.domain.useCases

import com.ntg.exchange.data.ExchangeRateRepository
import com.ntg.exchange.data.model.ExchangeRateDTO
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class GetExchangeRatesUseCaseImplTest {

    @Test
    fun `invoke should return exchange rate DTO from repository`() = runTest {
        // Given
        val expectedExchangeRateDTO = mockk<ExchangeRateDTO>()
        val repository = mockk<ExchangeRateRepository> {
            coEvery { getExchangeRates() } returns expectedExchangeRateDTO
        }
        val useCase = GetExchangeRatesUseCaseImpl(repository, Dispatchers.Default)

        // When
        val result = useCase.invoke()

        // Then
        assertEquals(expectedExchangeRateDTO, result)
    }

    @Test
    fun `invoke should return null if an exception is thrown in repository`() = runTest {
        // Given
        val repository = mockk<ExchangeRateRepository> {
            coEvery { getExchangeRates() } throws Exception("Error fetching data")
        }
        val useCase = GetExchangeRatesUseCaseImpl(repository, Dispatchers.Default)

        // When
        val result = useCase.invoke()

        // Then
        assertEquals(null, result)
    }
}
