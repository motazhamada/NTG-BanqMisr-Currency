package com.ntg.exchange.data

import com.ntg.exchange.data.mapper.toDomainModel
import com.ntg.exchange.data.remote.ExchangeRatesRemoteDataSource
import com.ntg.exchange.data.remote.model.ExchangeRateRemoteDTO
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestDispatcher
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class ExchangeRateRepositoryImplTest {

    private lateinit var repository: ExchangeRateRepositoryImpl
    private lateinit var mockDataSource: ExchangeRatesRemoteDataSource
    private lateinit var testDispatcher: TestDispatcher

    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun setUp() {
        mockDataSource = mockk(relaxed = true)
        testDispatcher = UnconfinedTestDispatcher()
        repository = ExchangeRateRepositoryImpl(mockDataSource, testDispatcher)
    }

    @Test
    fun `getExchangeRates() should call remoteDataSource and return mapped ExchangeRateDTO`() =
        runTest {
            // Given
            val date = "2023-08-05"
            val mockExchangeRateDTO = ExchangeRateRemoteDTO(
                success = true,
                rates = mapOf(
                    "USD" to 1.0,
                ),
                date = date,
                base = "EUR",
                historical = false,
                timestamp = 123132412,
            )
            coEvery { mockDataSource.invoke() } returns mockExchangeRateDTO

            // When
            val result = repository.getExchangeRates()

            // Then
            coVerify { mockDataSource.invoke() }
            assertEquals(false, result.historical)
            assertEquals(mockExchangeRateDTO.toDomainModel(), result)
        }

    @Test
    fun `getHistoricalExchangeRates() should call remoteDataSource and return mapped ExchangeRateDTO`() =
        runTest {
            // Given
            val date = "2023-08-01"
            val mockExchangeRateDTO = ExchangeRateRemoteDTO(
                success = true,
                rates = mapOf(
                    "USD" to 1.0,
                ),
                date = date,
                base = "EUR",
                historical = true,
                timestamp = 123132412,
            )
            coEvery { mockDataSource.historicalExchangeRates(date) } returns mockExchangeRateDTO

            // When
            val result = repository.getHistoricalExchangeRates(date)

            // Then
            coVerify { mockDataSource.historicalExchangeRates(date) }
            assertEquals(true, result.historical)
            assertEquals(mockExchangeRateDTO.toDomainModel(), result)
        }
}
