package com.ntg.exchange.data.remote

import com.ntg.exchange.data.remote.model.ExchangeRateRemoteDTO
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestDispatcher
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class ExchangeRatesRemoteDataSourceImplTest {

    private lateinit var dataSource: ExchangeRatesRemoteDataSourceImpl
    private lateinit var mockService: ExchangeRatesApiService
    private lateinit var testDispatcher: TestDispatcher

    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun setUp() {
        mockService = mockk(relaxed = true)
        testDispatcher = UnconfinedTestDispatcher()
        dataSource = ExchangeRatesRemoteDataSourceImpl(testDispatcher, mockService)
    }

    @Test
    fun `invoke() should call getLatestExchangeRates() from service`() = runTest {
        // When
        dataSource.invoke()

        // Then
        coVerify { mockService.getLatestExchangeRates() }
    }

    @Test
    fun `invoke() should return ExchangeRateRemoteDTO from service`() = runTest {
        // Given
        val expectedExchangeRateDTO = mockk<ExchangeRateRemoteDTO>()
        coEvery { mockService.getLatestExchangeRates() } returns expectedExchangeRateDTO

        // When
        val result = dataSource.invoke()

        // Then
        assertEquals(expectedExchangeRateDTO, result)
    }

    @Test
    fun `historicalExchangeRates() should call getHistoricalExchangeRates() from service`() =
        runTest {
            // Given
            val date = "2023-08-01"

            // When
            dataSource.historicalExchangeRates(date)

            // Then
            coVerify { mockService.getHistoricalExchangeRates(date) }
        }

    @Test
    fun `historicalExchangeRates() should return ExchangeRateRemoteDTO from service`() = runTest {
        // Given
        val date = "2023-08-01"
        val expectedExchangeRateDTO = mockk<ExchangeRateRemoteDTO>()
        coEvery { mockService.getHistoricalExchangeRates(date) } returns expectedExchangeRateDTO

        // When
        val result = dataSource.historicalExchangeRates(date)

        // Then
        assertEquals(expectedExchangeRateDTO, result)
    }
}
