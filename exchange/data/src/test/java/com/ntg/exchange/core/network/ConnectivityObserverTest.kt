package com.ntg.exchange.core.network

import android.content.Context
import android.net.ConnectivityManager
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class ConnectivityObserverTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var mockContext: Context
    private lateinit var mockConnectivityManager: ConnectivityManager

    @Before
    fun setUp() {
        mockContext = mockk(relaxed = true)
        mockConnectivityManager = mockk(relaxed = true)
        every { mockContext.getSystemService(Context.CONNECTIVITY_SERVICE) } returns mockConnectivityManager
    }

    @Test
    fun `Given ConnectivityObserver is created, Then initial connectivity state is null`() {
        // Given
        val connectivityObserver = ConnectivityObserver(mockContext)

        // Then
        assertEquals(null, connectivityObserver.isConnected.value)
    }
}
