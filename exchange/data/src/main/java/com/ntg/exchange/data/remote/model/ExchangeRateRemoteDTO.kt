package com.ntg.exchange.data.remote.model

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class ExchangeRateRemoteDTO(
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("historical")
    val historical: Boolean = false,
    @SerializedName("timestamp")
    val timestamp: Int,
    @SerializedName("date")
    val date: String,
    @SerializedName("base")
    val base: String,
    @SerializedName("rates")
    val rates: Map<String, Double>,
)
