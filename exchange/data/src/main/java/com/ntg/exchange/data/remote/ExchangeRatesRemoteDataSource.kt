package com.ntg.exchange.data.remote

import com.ntg.exchange.data.remote.model.ExchangeRateRemoteDTO
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

interface ExchangeRatesRemoteDataSource {
    suspend operator fun invoke(): ExchangeRateRemoteDTO
    suspend fun historicalExchangeRates(date: String): ExchangeRateRemoteDTO
}

internal class ExchangeRatesRemoteDataSourceImpl @Inject constructor(
    private val dispatcher: CoroutineDispatcher,
    private val service: ExchangeRatesApiService,
) : ExchangeRatesRemoteDataSource {

    override suspend fun invoke(): ExchangeRateRemoteDTO {
        return withContext(dispatcher) {
            service.getLatestExchangeRates()
        }
    }

    override suspend fun historicalExchangeRates(date: String): ExchangeRateRemoteDTO {
        return withContext(dispatcher) {
            service.getHistoricalExchangeRates(date)
        }
    }
}
