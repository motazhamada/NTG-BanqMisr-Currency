package com.ntg.exchange.data

import com.ntg.exchange.data.mapper.toDomainModel
import com.ntg.exchange.data.model.ExchangeRateDTO
import com.ntg.exchange.data.remote.ExchangeRatesRemoteDataSource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext

interface ExchangeRateRepository {
    suspend fun getExchangeRates(): ExchangeRateDTO
    suspend fun getHistoricalExchangeRates(date: String): ExchangeRateDTO
}

class ExchangeRateRepositoryImpl(
    private val exchangeRatesRemoteDataSource: ExchangeRatesRemoteDataSource,
    private val dispatcher: CoroutineDispatcher,
) : ExchangeRateRepository {

    override suspend fun getExchangeRates(): ExchangeRateDTO {
        return withContext(dispatcher) {
            exchangeRatesRemoteDataSource().toDomainModel()
        }
    }

    override suspend fun getHistoricalExchangeRates(date: String): ExchangeRateDTO {
        return withContext(dispatcher) {
            exchangeRatesRemoteDataSource.historicalExchangeRates(date).toDomainModel()
        }
    }
}
