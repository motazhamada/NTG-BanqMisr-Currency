package com.ntg.exchange.data.model

import androidx.annotation.Keep

@Keep
data class ExchangeRateDTO(
    val success: Boolean,
    val historical: Boolean,
    val timestamp: Int,
    val date: String,
    val base: String,
    val rates: Map<String, Double>,
)
