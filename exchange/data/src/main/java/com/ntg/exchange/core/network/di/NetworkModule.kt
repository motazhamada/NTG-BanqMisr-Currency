package com.ntg.exchange.core.network.di

import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.ntg.exchange.core.ServiceFactory
import com.ntg.exchange.core.network.interceptors.PayPayHeadersInterceptor
import com.ntg.exchange.core.retrofit.RetrofitServiceFactory
import com.ntg.exchange.data.BuildConfig
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.logging.HttpLoggingInterceptor
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Singleton
    @Provides
    internal fun provideServiceFactory(
        chuckInterceptor: ChuckerInterceptor,
    ): ServiceFactory {
        return RetrofitServiceFactory(
            baseUrl = BuildConfig.BASE_URL,
            interceptors = listOf(
                chuckInterceptor,
                PayPayHeadersInterceptor(),
                HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY),
            ),
        )
    }
}
