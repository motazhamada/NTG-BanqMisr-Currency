package com.ntg.exchange.core.retrofit

import androidx.annotation.Keep
import com.ntg.exchange.core.ServiceFactory
import okhttp3.Interceptor
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.HTTP
import retrofit2.http.PATCH
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.Url
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeUnit.SECONDS

/**
 * Create an implementation of the API endpoints defined by a service interface.
 *
 * The relative path for a given method is obtained from an annotation on the method describing
 * the request type. The built-in methods are [GET], [PUT], [POST], [PATCH], and [DELETE].
 * You can use a custom HTTP method with [HTTP]. For a dynamic URL, omit the path on the annotation
 * and annotate the first parameter with [Url]
 *
 * Method parameters can be used to replace parts of the URL by annotating them with [Path].
 * Replacement sections are denoted by an identifier surrounded by curly braces (e.g., "{foo}").
 * To add items to the query string of a URL use [Query].
 *
 * @param baseUrl the API base URL.
 * @param interceptors list of interceptors that observe the full span of each call.
 */
@Keep
class RetrofitServiceFactory(
    internal val baseUrl: String,
    internal val timeout: Long = 60L,
    internal val timeoutUnit: TimeUnit = SECONDS,
    internal val interceptors: List<Interceptor> = emptyList(),
) : ServiceFactory {

    private val retrofit by retrofitInitializer()

    override fun <T> create(serviceClass: Class<T>): T = retrofit.create(serviceClass)
}
