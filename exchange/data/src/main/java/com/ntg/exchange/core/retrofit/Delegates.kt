package com.ntg.exchange.core.retrofit

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Used to hide configurations & implementation from public use when obfuscation is applied
 */
internal fun RetrofitServiceFactory.retrofitInitializer() = lazy { createRetrofit() }

/**
 * Creates the [Retrofit] instance using the configured values.
 *
 * Note: If neither [Retrofit.Builder.client] nor [Retrofit.Builder.callFactory] is called,
 * a default [OkHttpClient] will be created and used.
 */
private fun RetrofitServiceFactory.createRetrofit(): Retrofit {
    return Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .client(createOkHttpClient())
        .baseUrl(baseUrl)
        .build()
}

/**
 * Create the HTTP client used for requests.
 */
private fun RetrofitServiceFactory.createOkHttpClient(): OkHttpClient {
    return OkHttpClient.Builder()
        .apply { interceptors.forEach(::addInterceptor) }
        .connectTimeout(timeout, timeoutUnit)
        .writeTimeout(timeout, timeoutUnit)
        .readTimeout(timeout, timeoutUnit)
        .build()
}
