package com.ntg.exchange.data.mapper

import com.ntg.exchange.data.model.ExchangeRateDTO
import com.ntg.exchange.data.remote.model.ExchangeRateRemoteDTO

fun ExchangeRateRemoteDTO.toDomainModel() =
    ExchangeRateDTO(
        success = success,
        date = date,
        historical = historical,
        timestamp = timestamp,
        base = base,
        rates = rates,
    )
