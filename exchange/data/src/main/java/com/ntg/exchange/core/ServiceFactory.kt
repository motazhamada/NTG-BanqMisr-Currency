package com.ntg.exchange.core

import androidx.annotation.Keep
import com.ntg.exchange.core.retrofit.RetrofitServiceFactory

/**
 * A quick creation experience provided in a Factory fashion to enable users to create network
 * service instances with encapsulated simple steps.
 *
 * @see RetrofitServiceFactory
 */
@Keep
interface ServiceFactory {

    /**
     * Create an implementation of the API endpoints defined by the [serviceClass] interface.
     * NOTE: API declarations must be interfaces.
     *
     * @param serviceClass the API declarations interface
     * @return the concrete implementation of [serviceClass]
     */
    fun <T> create(serviceClass: Class<T>): T
}
