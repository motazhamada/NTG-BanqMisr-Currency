package com.ntg.currency.utils.bindingAdapters
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import androidx.databinding.BindingAdapter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@BindingAdapter("defaultValue", "debounceTime")
fun EditText.setDefaultValue(value: Double, debounceTime: Long = 1000L) {
    var previousValue: String = value.toString()
    var debounceJob: Job? = null

    fun setDefaultValueIfEmpty(text: String?) {
        if (text.isNullOrEmpty() || text.trim().isEmpty() || (text.toDoubleOrNull() ?: 0.0) < 0.0) {
            setText("1.0")
        }
    }

    // Set the initial value
    setDefaultValueIfEmpty(previousValue)

    // Add a TextWatcher to handle changes to the text
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            // Check if the current value is different from the previous value
            if (s.toString() != previousValue) {
                previousValue = s.toString()
                // If the new value is not empty, blank, or less than 0, update the previous value and return
                if (!s.isNullOrEmpty() && s.trim().isNotEmpty() && (
                        s.toString().toDoubleOrNull()
                            ?: 0.0
                        ) >= 0.0
                ) {
                    previousValue = s.toString()
                    return
                }

                // If the new value is empty, blank, or less than 0, use a coroutine delay
                // before setting the default value
                debounceJob?.cancel() // Cancel the previous job if any
                debounceJob = CoroutineScope(Dispatchers.Main).launch {
                    delay(debounceTime) // Delay for the debounce time
                    setDefaultValueIfEmpty(s.toString())
                }
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
    })
}
