package com.ntg.currency.utils.bindingAdapters

import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.databinding.BindingAdapter
import com.ntg.exchange.presentation.OnItemSelectedListener

@BindingAdapter("itemsList", "onItemSelected", "observeSelected", requireAll = false)
fun Spinner.setSpinnerItemsAndListener(
    items: List<String>?,
    listener: OnItemSelectedListener?,
    selectedValue: String?,
) {
    // Check if the adapter is already set
    if (this.adapter == null && items != null) {
        val adapter = ArrayAdapter(this.context, android.R.layout.simple_spinner_item, items)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        this.adapter = adapter

        val itemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long,
            ) {
                listener?.onItemSelected(items[position])
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        this.onItemSelectedListener = itemSelectedListener

        // Set the selected value if provided
        if (selectedValue != null) {
            this.setSelection(items.indexOf(selectedValue))
        }
    } else {
        // The adapter is already set, so update the selected value if provided
        if (selectedValue != null) {
            val onItemSelectedListener = this.onItemSelectedListener

            @Suppress("UNCHECKED_CAST")
            val itemsAdapter = this.adapter as ArrayAdapter<String>
            this.onItemSelectedListener = null
            this.setSelection(itemsAdapter.getPosition(selectedValue))
            this.onItemSelectedListener = onItemSelectedListener
        }
    }
}
