package com.ntg.currency.details.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ntg.currency.R
import com.ntg.currency.databinding.ItemCurrencyConversionBinding
import com.ntg.currency.databinding.ItemHeaderBinding
import com.ntg.exchange.presentation.model.CurrencyConversion
import com.ntg.exchange.presentation.model.HistoricalExchangeRateData

class HistoricalExchangeRateAdapter :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var currencyFrom: String
    private lateinit var currencyTo: String
    val combinedList: MutableList<Any> = mutableListOf()

    fun setData(newData: List<HistoricalExchangeRateData>, currencyFrom: String, currencyTo: String) {
        this.currencyFrom = currencyFrom
        this.currencyTo = currencyTo
        combinedList.clear()
        for (item in newData) {
            // Add the header object to the combined list
            combinedList.add(item.date)
            combinedList.add(item.rates)
        }
        notifyItemRangeInserted(0, combinedList.size)
    }

    override fun getItemViewType(position: Int): Int {
        return if (combinedList[position]is String) VIEW_TYPE_HEADER else VIEW_TYPE_ITEM
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return if (viewType == VIEW_TYPE_HEADER) {
            val binding = ItemHeaderBinding.inflate(inflater, parent, false)
            HeaderViewHolder(binding)
        } else {
            val binding = ItemCurrencyConversionBinding.inflate(inflater, parent, false)
            ItemViewHolder(binding)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = combinedList[position]
        if (holder is HeaderViewHolder) {
            holder.bindHeader(item as String)
        } else if (holder is ItemViewHolder) {
            @Suppress("UNCHECKED_CAST")
            holder.bindItem(item as List<CurrencyConversion>)
        }
    }

    override fun getItemCount(): Int {
        return combinedList.size
    }

    companion object {
        private const val VIEW_TYPE_HEADER = 0
        private const val VIEW_TYPE_ITEM = 1
    }

    inner class HeaderViewHolder(private val binding: ItemHeaderBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindHeader(date: String) {
            // Bind the date to the header view
            binding.tvDate.text = binding.tvDate.context.getString(R.string.date_yyyy_mm_dd, date)
        }
    }

    inner class ItemViewHolder(private val binding: ItemCurrencyConversionBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindItem(items: List<CurrencyConversion>) {
            // Bind the data to the item view
            val currencyBase = items.firstOrNull {
                it.currency == currencyFrom
            } ?: items.first()
            val currency = items.firstOrNull {
                it.currency == currencyTo
            } ?: items.first()

            val convertedAmount = currency.amount / currencyBase.amount
            val formattedText = "${String.format("%.2f", convertedAmount)} ${currency.currency}"
            binding.tvRate.text = formattedText
        }
    }
}
