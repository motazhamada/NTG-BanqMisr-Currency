package com.ntg.currency.details.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ntg.currency.databinding.ItemCurrencyConversionBinding
import com.ntg.exchange.presentation.model.CurrencyConversion

class PopularExchangeRateAdapter :
    RecyclerView.Adapter<PopularExchangeRateAdapter.PopularCurrencyViewHolder>() {

    private lateinit var currencyFrom: String
    private lateinit var currencyTo: String
    val data: MutableList<CurrencyConversion> = mutableListOf()

    fun setData(newData: List<CurrencyConversion>, currencyFrom: String, currencyTo: String) {
        this.currencyFrom = currencyFrom
        this.currencyTo = currencyTo
        data.clear()
        val currencyBase = newData.firstOrNull { it.currency == currencyFrom }?.amount ?: 1.0
        data.addAll(
            newData.filter { it.currency in popularCurrencies && it.currency != currencyFrom && it.currency != currencyTo }
                .take(10).onEach { currency ->
                    currency.amount = currency.amount / currencyBase
                },
        )
        notifyItemRangeInserted(0, data.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PopularCurrencyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemCurrencyConversionBinding.inflate(inflater, parent, false)
        return PopularCurrencyViewHolder(binding)
    }

    override fun onBindViewHolder(
        holder: PopularCurrencyViewHolder,
        position: Int,
        payloads: MutableList<Any>,
    ) {
        val item = data[position]
        holder.bindItem(item)
    }

    override fun onBindViewHolder(holder: PopularCurrencyViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {
        return data.size
    }

    companion object {
        private val popularCurrencies = listOf(
            "USD", // United States Dollar
            "EUR", // Euro
            "JPY", // Japanese Yen
            "GBP", // British Pound Sterling
            "CHF", // Swiss Franc
            "CAD", // Canadian Dollar
            "AUD", // Australian Dollar
            "CNY", // Chinese Yuan
            "SEK", // Swedish Krona
            "NZD", // New Zealand Dollar
            "KRW", // South Korean Won
            "SGD", // Singapore Dollar
        )
    }

    inner class PopularCurrencyViewHolder(private val binding: ItemCurrencyConversionBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindItem(item: CurrencyConversion) {
            // Bind the data to the item view
            val formattedText = "${String.format("%.2f", item.amount)} ${item.currency}"
            binding.tvRate.text = formattedText
        }
    }
}
