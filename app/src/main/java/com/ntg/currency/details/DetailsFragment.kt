package com.ntg.currency.details

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.ntg.currency.R
import com.ntg.currency.databinding.FragmentDetailsBinding
import com.ntg.currency.details.adapters.HistoricalExchangeRateAdapter
import com.ntg.currency.details.adapters.PopularExchangeRateAdapter
import com.ntg.exchange.presentation.CurrencyConversionViewModel
import com.ntg.exchange.presentation.model.HistoricalExchangeRateData
import com.ntg.exchange.presentation.state.HistoricalExchangeRateDataState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class DetailsFragment : Fragment(), LifecycleOwner {
    private lateinit var binding: FragmentDetailsBinding
    private val viewModel: CurrencyConversionViewModel by activityViewModels<CurrencyConversionViewModel>()

    private val historicalExchangeRateAdapter: HistoricalExchangeRateAdapter by lazy { HistoricalExchangeRateAdapter() }
    private val popularExchangeRateAdapter: PopularExchangeRateAdapter by lazy { PopularExchangeRateAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentDetailsBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerViews()
        binding.tvHistoricalTitle.text =
            getString(R.string.historical_exchange_rates, viewModel.currencyFrom.value!!)
        binding.tvPopularCurrenciesTitle.text =
            getString(R.string.popular_exchange_rates, viewModel.currencyFrom.value!!)
        lifecycleScope.launch {
            viewModel.historicalExchangeRatesState.collectLatest {
                when (it) {
                    is HistoricalExchangeRateDataState.Error -> {
                        viewModel.setLoadingState(false)
                        Toast.makeText(requireContext(), it.error.message, Toast.LENGTH_LONG).show()
                    }

                    HistoricalExchangeRateDataState.Initial -> {}
                    HistoricalExchangeRateDataState.Loading -> {
                        viewModel.setLoadingState(true)
                    }

                    is HistoricalExchangeRateDataState.Success -> {
                        historicalExchangeRateAdapter.setData(
                            it.data,
                            viewModel.currencyFrom.value!!,
                            viewModel.currencyTo.value!!,
                        )
                        popularExchangeRateAdapter.setData(
                            it.data.first().rates,
                            viewModel.currencyFrom.value!!,
                            viewModel.currencyTo.value!!,
                        )
                        bindChartData(it.data)
                        viewModel.setLoadingState(false)
                    }
                }
            }
        }
        viewModel.performGetHistoricalExchangeRatesData()
    }

    private fun setupRecyclerViews() {
        binding.rvHistoricalData.layoutManager = LinearLayoutManager(requireContext())
        binding.rvHistoricalData.adapter = historicalExchangeRateAdapter
        binding.rvPopularCurrenciesData.layoutManager = LinearLayoutManager(requireContext())
        binding.rvPopularCurrenciesData.adapter = popularExchangeRateAdapter
    }

    private fun bindChartData(items: List<HistoricalExchangeRateData>) {
        val entries: MutableList<Entry> = mutableListOf()

        // Assuming items are sorted by date, otherwise, you may need to sort them here
        for (i in items.indices) {
            val currencyBase = items[i].rates.firstOrNull {
                it.currency == viewModel.currencyFrom.value
            } ?: items[i].rates.first()
            val currency = items[i].rates.firstOrNull {
                it.currency == viewModel.currencyTo.value
            } ?: items[i].rates.first()
            val convertedAmount = currency.amount / currencyBase.amount
            entries.add(Entry(i.toFloat(), convertedAmount.toFloat()))
        }

        val lineDataSet = LineDataSet(entries, "Currency Conversion").apply {
            color = Color.BLUE
            valueTextColor = Color.BLACK
            fillAlpha = 2
            mode = LineDataSet.Mode.LINEAR
            lineWidth = 2f
            setDrawHorizontalHighlightIndicator(false)
            setDrawVerticalHighlightIndicator(false)
            setDrawCircleHole(false)
        }

        val dataSets: MutableList<ILineDataSet> = mutableListOf()
        dataSets.add(lineDataSet)

        val lineData = LineData(dataSets)
        binding.chart.apply {
            data = lineData
            setDrawGridBackground(false)
            setDrawBorders(false)
            description.text = "Currency Conversion Chart"
            axisLeft.setDrawGridLines(false)
            axisRight.setDrawGridLines(false)
            xAxis.isEnabled = false
            description = null
            legend.isEnabled = false
            // for animation
            animateXY(1000, 1000, Easing.EaseInCubic)
            invalidate()
        }
    }
}
