package com.ntg.currency.converter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.ntg.currency.databinding.FragmentConverterBinding
import com.ntg.exchange.presentation.CurrencyConversionViewModel
import com.ntg.exchange.presentation.state.ExchangeRateDataState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import java.util.concurrent.TimeUnit

@AndroidEntryPoint
class ConverterFragment : Fragment() {
    private var binding: FragmentConverterBinding? = null
    private val viewModel: CurrencyConversionViewModel by activityViewModels<CurrencyConversionViewModel>()

    private var exchangeRatesJob: Job? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentConverterBinding.inflate(inflater, container, false)
        binding?.lifecycleOwner = viewLifecycleOwner
        binding?.viewModel = viewModel
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleScope.launch {
            viewModel.exchangeRatesDataState.collectLatest {
                when (it) {
                    is ExchangeRateDataState.Error -> {
                        viewModel.setLoadingState(false)
                        Toast.makeText(requireContext(), it.error.message, Toast.LENGTH_LONG).show()
                    }

                    ExchangeRateDataState.Initial -> {}
                    ExchangeRateDataState.Loading -> {
                        viewModel.setLoadingState(true)
                    }

                    is ExchangeRateDataState.Success -> {
                        viewModel.setLoadingState(false)
                    }
                }
            }
        }
        exchangeRatesJob = lifecycleScope.launch {
            while (true) {
                viewModel.performGetExchangeRatesData()
                delay(TimeUnit.MINUTES.toMillis(6))
            }
        }
        binding?.btnDetails?.setOnClickListener {
            val action = ConverterFragmentDirections.actionConverterFragmentToDetailsFragment()
            findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        // Cancel the job when the fragment is destroyed
        exchangeRatesJob?.cancel()
        binding = null
    }
}
